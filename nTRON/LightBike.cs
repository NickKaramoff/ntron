﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace nTRON
{
    public class LightBike
    {
        public Rectangle bike;
        public Color bikecolor;
        Brush b;
        public int dir = 0;
        public bool live = true;
        public struct Vector
        {
            public int vert;
            public int hor;
        }
        Vector vect;

        public LightBike(Point start, Size size, Color bcolor)
        {
            bike = new Rectangle(start, size);
            bikecolor = bcolor;
            b = new SolidBrush(bikecolor);
        }

        public void BikeDraw(Graphics gs)
        {
            b = new SolidBrush(bikecolor);
            gs.FillRectangle(b, bike);
            
        }

        public void Update()
        {
            bike.X += vect.hor;
            bike.Y -= vect.vert;
        }

        public void VectUp()
        {
            vect.vert = 1;
            vect.hor = 0;
            dir = 1;
        }
        public void VectDown()
        {
            vect.vert = -1;
            vect.hor = 0;
            dir = 3;
        }
        public void VectRight()
        {
            vect.vert = 0;
            vect.hor = 1;
            dir = 2;
        }
        public void VectLeft()
        {
            vect.vert = 0;
            vect.hor = -1;
            dir = 4;
        }
    }
}
